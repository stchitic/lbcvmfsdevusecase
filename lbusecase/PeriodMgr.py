#!/usr/bin/env python
"""
Simulate installations for a day for one project

"""
from __future__ import print_function
from timeit import default_timer as timer
import argparse
import logging
import os
import shutil

from Generator import KiB, MiB
from Generator import generate


def install_period(target, current_date, transaction_index,  transaction_size, mu, sigma, dryrun=False):
    """ Install, cleanup as needed """

    current_period_dirname = "per_%d" % current_date
    current_trans_dirname = "trans_%d" % transaction_index

    current_period_dir = os.path.join(target, current_period_dirname)
    current_trans_dir = os.path.join(current_period_dir, current_trans_dirname)

    logging.info("Generating transaction %d, %d MiB data for the day %s" % (
        transaction_index, transaction_size / MiB, current_period_dirname))
    start = timer()
    generate(current_trans_dir, transaction_size, mu, sigma)
    end = timer()
    logging.info("%s creation time: %s s", current_period_dirname, end - start)


def cleanup(target, current_date, period):
    """ Install, cleanup as needed """
    latest_to_keep = current_date - period + 1
    dirstokeep = set(["per_%d" % d for d in range(
        latest_to_keep, current_date + 1)])
    alldirs = set([d for d in os.listdir(target) if d.startswith("per_")])

    toremove = alldirs - dirstokeep
    for r in toremove:
        period_dir = os.path.join(target, r)
        if os.path.exists(period_dir):
            logging.info("Removing %s" % period_dir)
            start = timer()
            shutil.rmtree(period_dir)
            end = timer()
            logging.info("%s removal time: %s s",
                         r, end - start)

